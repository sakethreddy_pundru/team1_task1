//
//  ViewController.m
//  Team1_Task1
//
//  Created by admin on 19/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController{

    UIImage *image;
    UIImageView *imageView;
    NSMutableArray *mainArray;
    int index;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    index =0;
    
    mainArray = [[NSMutableArray alloc]initWithObjects:@"Image1.png",@"Image2.png",@"Image 3.png",@"Image 4.png", nil];
    
    UIImage *pic = [UIImage imageNamed:@"Image1.png"];
    imageView = [[UIImageView alloc]initWithImage:pic];
    imageView.frame = self.view.frame;
    [imageView setImage:[UIImage imageNamed:[mainArray objectAtIndex:index]]];
    [self.view addSubview:imageView];
    imageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]init];
    gesture.numberOfTapsRequired = 1;
    [gesture addTarget:self action:@selector(tap)];
    [imageView addGestureRecognizer:gesture];
    
    
}


-(void)tap{
    
    
    ++index;
    
    if (index == mainArray.count)
    {
        index = 0 ;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:@"You reached maximum number of images" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];

    }
    
    [imageView setImage:[UIImage imageNamed:[mainArray objectAtIndex:index]]];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }

@end
